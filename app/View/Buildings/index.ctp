<!--Buildings--->
<h2> All Buildings </h2>

<table>
	<tr>
		<th>Building Name</th>
		<th>QR name</th>
	</tr>
	<?php foreach($allBuildings as $building): ?>
	<tr>
		<td><?php echo $building['Building']['full_name']; ?></td>
		<td><?php echo $building['Building']['name']; ?></td>
	</tr>	
	<?php endforeach; ?>
</table>