-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 11, 2012 at 06:52 PM
-- Server version: 5.1.61
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `k11943_Dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE IF NOT EXISTS `buildings` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `directions` text,
  `date_built` date DEFAULT NULL,
  `description` text,
  `notes` text,
  `full_name` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`id`, `name`, `directions`, `date_built`, `description`, `notes`, `full_name`) VALUES
(0, 'SCNCE', 'ECSU main campus.', NULL, NULL, 'QR test facility', 'Science Building'),
(0, 'std_name', 'dirs', NULL, 'descr', NULL, 'f_name'),
(0, 'std_name2', '', NULL, '', NULL, 'f_name2');

-- --------------------------------------------------------

--
-- Table structure for table `itemIdents`
--

CREATE TABLE IF NOT EXISTS `itemIdents` (
  `id_itemIdent` int(11) NOT NULL,
  PRIMARY KEY (`id_itemIdent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itemIdents`
--

INSERT INTO `itemIdents` (`id_itemIdent`) VALUES
(1),
(2),
(3),
(4),
(5);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id_item` int(11) NOT NULL,
  `num_serial` varchar(45) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `notes` varchar(45) DEFAULT NULL,
  `condition` varchar(45) DEFAULT NULL,
  `date_manufactured` date DEFAULT NULL,
  `inspect_interval` varchar(45) DEFAULT NULL,
  `date_last_inspect` date DEFAULT NULL,
  `last_inspect_person` varchar(45) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  `itemIdent_id_itemIdent` int(11) NOT NULL,
  `room_num_room` varchar(45) NOT NULL,
  PRIMARY KEY (`num_serial`,`itemIdent_id_itemIdent`,`room_num_room`),
  KEY `fk_item_itemIdent1` (`itemIdent_id_itemIdent`),
  KEY `fk_item_room1` (`room_num_room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id_item`, `num_serial`, `type`, `priority`, `description`, `notes`, `condition`, `date_manufactured`, `inspect_interval`, `date_last_inspect`, `last_inspect_person`, `size`, `itemIdent_id_itemIdent`, `room_num_room`) VALUES
(1, '11111', 'Fire Extinguisher', NULL, 'CO2 Fire Extinguisher', 'test', 'good', NULL, 'Monthly', '2012-03-14', 'Joe', 'small', 1, '137'),
(2, '11112', 'Fire Extinguisher', NULL, 'CO2 Fire Extinguisher', NULL, 'good', NULL, 'Monthly', '2012-03-14', 'joe', 'small', 2, '137');

-- --------------------------------------------------------

--
-- Table structure for table `paths`
--

CREATE TABLE IF NOT EXISTS `paths` (
  `id_path` int(11) NOT NULL,
  `name` text NOT NULL,
  `current` text NOT NULL,
  `next` text,
  `previous` text,
  PRIMARY KEY (`id_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paths`
--

INSERT INTO `paths` (`id_path`, `name`, `current`, `next`, `previous`) VALUES
(1, 'TestPath', 'B_SCNCE', 'R_137', NULL),
(2, 'TestPath', 'R_137', 'II_1', 'B_SCNCE'),
(3, 'TestPath', 'II_1', 'I_11111', 'R_137'),
(4, 'TestPath', 'I_11111', 'II_2', 'II_1'),
(5, 'TestPath', 'II_2', 'I_11112', 'I_11111'),
(6, 'TestPath', 'I_11112', 'R_139', 'II_2'),
(7, 'TestPath', 'R_139', 'R_138', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id_room` int(11) NOT NULL,
  `num_room` varchar(45) NOT NULL,
  `directions` text,
  `description` text,
  `notes` text,
  `building_name` varchar(45) NOT NULL,
  PRIMARY KEY (`num_room`,`building_name`),
  KEY `fk_room_building1` (`building_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id_room`, `num_room`, `directions`, `description`, `notes`, `building_name`) VALUES
(1, '137', 'West Wing', 'Tech Lab', NULL, 'SCNCE'),
(3, '138', 'West Wing', 'Computer Lab', NULL, 'SCNCE'),
(2, '139', 'West Wing', 'Computer Lab', NULL, 'SCNCE');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `fk_item_itemIdent1` FOREIGN KEY (`itemIdent_id_itemIdent`) REFERENCES `itemIdents` (`id_itemIdent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_room1` FOREIGN KEY (`room_num_room`) REFERENCES `rooms` (`num_room`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `fk_room_building1` FOREIGN KEY (`building_name`) REFERENCES `buildings` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
